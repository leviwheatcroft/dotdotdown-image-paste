/* eslint-env browser */
/* global $ dotdotdown */

import styles from './styles'
import layout from './layout'
import 'paste.js'
const {
  Component,
  Alert,
  Uri,
  upload,
  registerPlugin
} = dotdotdown

export default class ImagePaste extends Component {
  constructor ({ dataURL, blob }) {
    super(layout, styles)
    const uri = new Uri()
    const parentDir = uri.dir
    const match = /image\/(\w{2,4});/.exec(dataURL)
    const ext = match ? match[1] : 'jpg'
    const destPath = new Uri()
    destPath.setBase(`image.${ext}`)
    Object.assign(this, { dataURL, parentDir, ext, destPath, blob })
  }
  render () {
    const {
      dataURL,
      destPath
    } = this
    super.render({ dataURL, destPath: destPath.getUriHuman() })
    Alert.load($('.alert-container', this.el))
    this.el.on('hidden.bs.modal', () => this.el.remove())
    return this
  }
  inputFileName (event) {
    this.destPath.setBaseHuman(`${$(event.target).val()}.${this.ext}`)
    $('input[name=destPath]').val(this.destPath.getUriHuman())
  }
  async clickSubmit (event) {
    event.preventDefault()
    const {
      blob,
      destPath
    } = this
    const codemirror = $('.editor').component().editor.codemirror

    let result
    try {
      const data = new FormData()
      data.append('file', blob)
      data.append('path', destPath.getUri())
      result = await upload(data)
      const description = codemirror.getSelection() || destPath.getBaseHuman()
      const markdown = `![${description}](${destPath.relative()})`
      codemirror.replaceSelection(markdown)
      this.el.modal('hide')
    } catch (err) {
      $('.alert-container', this.el).component().addAlert({
        dismissable: true,
        context: 'danger',
        content: 'Error uploading image, see console'
      })
      console.log(err)
    }
  }
  static load () {
    // don't do anything unless there's an .editor element
    if ($('.editor').length === 0) return
    const input = $('.editor').component().editor.codemirror.getInputField()
    $(input).pastableTextarea().on('pasteImage', (_, data) => {
      new ImagePaste(data)
      .render()
      .el.appendTo($('body'))
      .modal('show')
    })
    .on('pasteImageError', (_, data) => {
      $('#container > .alert-container').component().addAlert({
        dismissable: true,
        content: 'pasteImageError, see console'
      })
      console.log(data)
      throw new Error('pasteImageError')
    })
  }
}

registerPlugin(ImagePaste)
